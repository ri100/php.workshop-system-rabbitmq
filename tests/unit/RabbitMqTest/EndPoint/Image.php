<?php
namespace RabbitMqTest\EndPoint;

// Pusher
use RabbitMqTest\Config\RabbitMQConfig;
use Workshop\System\RabbitMq\QueueEndPoint;

class Image extends QueueEndPoint {

    /**
     * QUEUE PER METHOD
     *
     * @param $param1
     * @param array $param2
     */
    public function upload($param1, array $param2) {
        $this->push(
            RabbitMQConfig::getConfig(),
            __FUNCTION__,
            func_get_args(),
            true,
            \RabbitMqTest\Consumer\Image::Type()
        );
    }

    /**
     * QUEUE PER METHOD
     *
     * @param $param1
     * @param array $param2
     */
    public function download($param1, array $param2) {
        $this->push(
            RabbitMQConfig::getConfig(),
            __FUNCTION__,
            func_get_args(),
            true,
            \RabbitMqTest\Consumer\Image::Type()
        );
    }

    /**
     * QUEUE PER CLASS
     *
     * @param $param1
     * @param array $param2
     */
    public function perClass($param1, $param2) {
        $this->push(
            RabbitMQConfig::getConfig(),
            __FUNCTION__,
            func_get_args(),
            false, // Queue per class
            \RabbitMqTest\Consumer\Image::Type()
        );
    }

}