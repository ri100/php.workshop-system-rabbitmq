<?php
namespace RabbitMqTest;

use RabbitMqTest\Config\RabbitMQConfig;

require_once __DIR__ . '/../../../../vendor/autoload.php';

$oDownstreamQueueTasks = new Consumer\Image();
$oDownstreamQueueTasks->consumeQueueForMethod(
    'upload',
    RabbitMQConfig::getConfig(),
    Consumer\Image::Type()
);