<?php
namespace RabbitMqTest;

require_once __DIR__ . '/../../../../vendor/autoload.php';

$oDownstreamQueueTasks = new Consumer\Image();
$oDownstreamQueueTasks->consumeQueueForMethod(
    'download', // Method
    Config\RabbitMQConfig::getConfig(), // Config
    Consumer\Image::Type(), // Queue, null means default
    function($bResult, $aParams) { var_dump($bResult); var_dump($aParams); }
);