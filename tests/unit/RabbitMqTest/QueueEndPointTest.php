<?php
namespace RabbitMqTest;

use Generi\Type;
use RabbitMqTest\EndPoint\Image;

require_once __DIR__ . '/../../../vendor/autoload.php';

class QueueEndPointTest extends \PHPUnit_Framework_TestCase {

    public function testQueue() {
        // Add to queue
        $oQueueTask = new Image();
        $oQueueTask->upload('upload1', array('item1'));
        $oQueueTask->download('download1', array('item2'));
        $oQueueTask->perClass('perClass', array(new Type()));
    }

}
