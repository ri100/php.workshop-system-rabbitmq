<?php
namespace RabbitMqTest\Consumer;

use Workshop\System\RabbitMq\QueueConsumer;

// Consumer - Worker
class Image extends QueueConsumer {

    public function download($param1, array $param2) {
        var_dump($param1);
        var_dump($param2);
        return false;
    }

    public function upload($param1, array $param2) {
        var_dump($param1);
        var_dump($param2);
        return false;
    }

    public function perClass($param1, $param2) {
        var_dump($param1);
        var_dump($param2);
        return false;
    }

}