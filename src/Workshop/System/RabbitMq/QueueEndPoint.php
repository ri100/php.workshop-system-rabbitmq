<?php
namespace Workshop\System\RabbitMq {

    use Generi\Object;

    abstract class QueueEndPoint extends Object {

        /**
         * Constructor must be parameter-less
         */
        final public function __construct() { }

        /**
         * @param QueueConfig $oConfig
         * @param bool $bQueuePerMethod
         * @return QueueHandler
         */
        private function getQueueHandler(QueueConfig $oConfig, $bQueuePerMethod = false) {
            $queueHandler = new QueueHandler($oConfig);
            $queueHandler->makeOneQueuePerMethod($bQueuePerMethod);
            return $queueHandler;
        }

        /**
         * Pushes job to the queue. Queue is named after the class name, job has a name of a function.
         *
         * @param $oConfig
         * @param $sJobName
         * @param $aArgs
         * @param bool $bQueuePerMethod
         * @param null $sQueueName
         */
        protected function push($oConfig, $sJobName, $aArgs, $bQueuePerMethod = false, $sQueueName = null) {

            if($sQueueName === null) {
                $sQueueName = $this->getType();
            }

            $this
                ->getQueueHandler($oConfig, $bQueuePerMethod)
                ->push($sQueueName, $sJobName, $aArgs);
        }

    }

}
