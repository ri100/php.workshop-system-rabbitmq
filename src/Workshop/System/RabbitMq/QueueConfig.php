<?php
namespace Workshop\System\RabbitMq {

    class QueueConfig {

        private $sHost = null;
        private $iPort = null;
        private $sUser = null;
        private $sPass = null;
        private $sVHost = '/';
	    private $bInsist = false;
	    private $sLoginMethod = 'AMQPLAIN';
        private $sLoginResponse = null;
        private $sLocale = 'en_US';
        private $iConnectionTimeout = 3;
        private $iReadWriteTimeout = 3;
        private $mContext = null;
        private $bKeepAlive = false;
        private $iHeartbeat = 0;

        public function __construct(
	        $sHost,
	        $iPort,
	        $sUser,
	        $sPass,
	        $sVHost = '/',
	        $bInsist = false,
	        $sLoginMethod = 'AMQPLAIN',
            $sLoginResponse = null,
            $sLocale = 'en_US',
            $iConnectionTimeout = 3,
	        $iReadWriteTimeout = 3,
	        $mContext = null,
            $bKeepAlive = false,
            $iHeartbeat = 0
        ) {
            $this->sHost = $sHost;
            $this->iPort = $iPort;
            $this->sUser = $sUser;
            $this->sPass = $sPass;
	        $this->sVHost = $sVHost;
	        $this->bInsist = $bInsist;
	        $this->sLoginMethod = $sLoginMethod;
	        $this->sLoginResponse = $sLoginResponse;
	        $this->sLocale = $sLocale;
            $this->iConnectionTimeout = $iConnectionTimeout;
	        $this->iReadWriteTimeout = $iReadWriteTimeout;
	        $this->mContext = $mContext;
            $this->bKeepAlive = $bKeepAlive;
            $this->iHeartbeat = $iHeartbeat;
        }

        /**
         * @return string | null
         */
        public function getHost() {
            return $this->sHost;
        }

        /**
         * @return string | null
         */
        public function getPort() {
            return $this->iPort;
        }

        /**
         * @return string | null
         */
        public function getUser() {
            return $this->sUser;
        }

        /**
         * @return string | null
         */
        public function getPass() {
            return $this->sPass;
        }

	    /**
	     * @return string
	     */
	    public function getVHost() {
		    return $this->sVHost;
	    }

	    /**
	     * @return boolean
	     */
	    public function getInsist() {
		    return $this->bInsist;
	    }

	    /**
	     * @return string
	     */
	    public function getLoginMethod() {
		    return $this->sLoginMethod;
	    }

	    /**
	     * @return null
	     */
	    public function getLoginResponse() {
		    return $this->sLoginResponse;
	    }

	    /**
	     * @return string
	     */
	    public function getLocale() {
		    return $this->sLocale;
	    }

	    /**
	     * @return int
	     */
	    public function getConnectionTimeout() {
		    return $this->iConnectionTimeout;
	    }

	    /**
	     * @return int
	     */
	    public function getReadWriteTimeout() {
		    return $this->iReadWriteTimeout;
	    }

	    /**
	     * @return null
	     */
	    public function getContext() {
		    return $this->mContext;
	    }

	    /**
	     * @return boolean
	     */
	    public function getKeepAlive() {
		    return $this->bKeepAlive;
	    }

	    /**
	     * @return int
	     */
	    public function getHeartbeat() {
		    return $this->iHeartbeat;
	    }
    }

}
