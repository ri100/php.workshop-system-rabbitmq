<?php
namespace Workshop\System\RabbitMq {

	use PhpAmqpLib\Connection\AMQPStreamConnection;
	use PhpAmqpLib\Message\AMQPMessage;

	class QueueHandler {

		const EXCHANGE = '';
		const SEPARATOR = ':';

		private $oConnection = null;

		/**
		 * @var null|QueueConfig
		 */
		private $oConnectionConfig = null;
		private $bQueuePerMethod = false;

		public function __construct(QueueConfig $config) {
			$this->oConnectionConfig = $config;
		}

		public function makeOneQueuePerMethod($bQueuePerMethod) {
			$this->bQueuePerMethod = $bQueuePerMethod;
		}

		private function getConnection() {
			if ($this->oConnection === null) {
				$this->oConnection = new AMQPStreamConnection(
					$this->oConnectionConfig->getHost(),
					$this->oConnectionConfig->getPort(),
					$this->oConnectionConfig->getUser(),
					$this->oConnectionConfig->getPass(),
					$this->oConnectionConfig->getVHost(),
					$this->oConnectionConfig->getInsist(),
					$this->oConnectionConfig->getLoginMethod(),
					$this->oConnectionConfig->getLoginResponse(),
					$this->oConnectionConfig->getLocale(),
					$this->oConnectionConfig->getConnectionTimeout(),
					$this->oConnectionConfig->getReadWriteTimeout(),
					$this->oConnectionConfig->getContext(),
					$this->oConnectionConfig->getKeepAlive(),
					$this->oConnectionConfig->getHeartbeat()
				);
			}
			return $this->oConnection;
		}

		public function encodeCall($sCall, array $aParams) {
			return json_encode(array($sCall, $aParams));
		}

		public function decodeCall($sCall, $bAssoc = false) {
			return json_decode($sCall, $bAssoc);
		}

		/**
		 * Pushes jod to the queue.
		 *
		 * @param $sQueueName
		 * @param $sJobName
		 * @param array $aParams
		 * @return bool
		 */
		public function push($sQueueName, $sJobName, array $aParams) {

			$oMessage = new AMQPMessage(
				$this->encodeCall($sJobName, $aParams),
				array('delivery_mode' => 2)
			);

			if ($this->bQueuePerMethod) {
				$sQueueName = $sQueueName . self::SEPARATOR . $sJobName;
			}

			$oConnection = $this->getConnection();
			$oChannel = $oConnection->channel();
			$oChannel->queue_declare($sQueueName, false, true, false, false);
			$oChannel->basic_publish($oMessage, self::EXCHANGE, $sQueueName);
			$oChannel->close();
			$oConnection->close();

			return true;
		}

		private function route($sQueueName, \Closure $oCallback, $sJobName = null) {

			if ($this->bQueuePerMethod && null !== $sJobName) {
				$sQueueName = $sQueueName . self::SEPARATOR . $sJobName;
			}

			$oChannel = $this->getConnection()->channel();
			$oChannel->queue_declare($sQueueName, false, true, false, false);
			$oChannel->basic_qos(null, 1, null);
			$oChannel->basic_consume($sQueueName, '', false, false, false, false, $oCallback);

			while (count($oChannel->callbacks)) {
				$oChannel->wait();
			}

			$oChannel->close();
		}

		/**
		 * @param $sQueue
		 * @param string | null $sClassName
		 * @param \Closure $onSuccess
		 * @param \Closure $onError
		 * @param string | null $sJobName
		 * @param \Closure $onJobAck
		 * @param \Closure $onJobReject
		 * @param bool $bDecodeParamsAssoc
		 * @param bool $bReQueue
		 */
		public function handleQueue(
			$sQueue,
			$sClassName = null,
			\Closure $onSuccess = null, // function($bResult, $aMethodParams)
			\Closure $onError = null, // function(\Exception $e)
			$sJobName = null,
			\Closure $onJobAck= null,  // function($mJobResult, \PhpAmqpLib\Channel\AMQPChannel $oRabbitMqChannel, array $sDeliveryInfo)
			\Closure $onJobReject = null, // function(\Exception $oException, \PhpAmqpLib\Channel\AMQPChannel $oRabbitMqChannel, array $sDeliveryInfo)
			$bDecodeParamsAssoc = false,
			$bReQueue = false
		) {

			$oEndPoint = $this;

			if ($sClassName !== null) {
				$sQueue = (string)$sClassName;
			}

			if ($this->bQueuePerMethod && null !== $sJobName) {
				$sQueue = $sQueue . self::SEPARATOR . $sJobName;
			}

			$this->route($sQueue,
				function (AMQPMessage $oMessage) use ($oEndPoint, $sQueue, $onSuccess, $onError, $onJobAck, $onJobReject, $bReQueue, $bDecodeParamsAssoc) {
					/** @var \PhpAmqpLib\Channel\AMQPChannel $channel */
					$channel = $oMessage->delivery_info['channel'];
					try {

						list($sMethod, $aMethodParams) = $oEndPoint->decodeCall($oMessage->body, $bDecodeParamsAssoc);
						if ($this->bQueuePerMethod) {
							list($sQueueNameIsClass) = explode(self::SEPARATOR, $sQueue);
						} else {
							$sQueueNameIsClass = $sQueue;
						}

						if (!class_exists($sQueueNameIsClass)) {
							throw new Exception(sprintf('Class %s does not exist', $sQueueNameIsClass));
						}

						$oQueueClass = new $sQueueNameIsClass();

						if (!method_exists($oQueueClass, $sMethod)) {
							throw new Exception(sprintf('Method %s:%s does not exist', $sQueueNameIsClass, $sMethod));
						}

						if (!$oQueueClass instanceof QueueConsumer) {
							throw new Exception(sprintf('Class %s is not an instance of QueueConsumer', $sQueueNameIsClass));
						}

						$aHandler = array($oQueueClass, $sMethod);
						if (!is_callable($aHandler)) {
							throw new Exception(sprintf('Method %s:%s is not callable', $sQueueNameIsClass, $sMethod));
						}

						// Run
						$bResult = call_user_func_array($aHandler, $aMethodParams);

						if(null !== $onJobAck) {
							$onJobAck($bResult, $channel, $oMessage->delivery_info);
						} else if (false === $bResult) {
							$channel->basic_reject($oMessage->delivery_info['delivery_tag'], $bReQueue);
						} else {
							$channel->basic_ack($oMessage->delivery_info['delivery_tag']);
						}

						if (null !== $onSuccess) {
							$onSuccess($bResult, $aMethodParams);
						}

					} catch (\Exception $e) {
						if(null !== $onJobReject) {
							$onJobReject($e, $channel, $oMessage->delivery_info);
						} else {
							$channel->basic_reject($oMessage->delivery_info['delivery_tag'], $bReQueue);
						}
						if (null !== $onError) {
							$onError($e);
						}

					}

				}
			);
		}

	}

}
