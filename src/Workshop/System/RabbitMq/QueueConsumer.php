<?php
namespace Workshop\System\RabbitMq {

    use Generi\Object;
	use Workshop\Application\Event\Handler;

	abstract class QueueConsumer extends Object {

	    private $oEventHandler;
		const TICKER_EVENT = '@Tick';

        /**
         * Constructor must be parameter-less
         */
        final public function __construct() {
	        $this->oEventHandler = new Handler();
        }

        /**
         * Default onExit handler.
         *
         * @return \Closure
         */
        protected function onExit() {
            return function () {
                error_log(sprintf('Queue class %s exited', __CLASS__));
            };
        }

		/**
		 * Consumes configured queue.
		 *
		 * @param string $sQueueMethod
		 * @param QueueConfig $oConfig Queue configuration
		 * @param null $sClassName
		 * @param \Closure $onSuccess Closure in form of function(bool result, array methodParams)
		 * @param \Closure $onError Runs on error
		 * @param \Closure $onJobAck Replaces standard basic_ack
		 * @param \Closure $onJobReject Replaces standard basic_reject
		 * @param bool $bDecodeParamsAssoc
		 * @param bool $bReQueue
		 */
        public function consumeQueueForMethod(
	        $sQueueMethod,
	        QueueConfig $oConfig,
	        $sClassName = null,
	        \Closure $onSuccess = null,
	        \Closure $onError = null,
	        \Closure $onJobAck= null,  // function($mJobResult, \PhpAmqpLib\Channel\AMQPChannel $oRabbitMqChannel, array $sDeliveryInfo)
	        \Closure $onJobReject = null, // function(\Exception $oException, \PhpAmqpLib\Channel\AMQPChannel $oRabbitMqChannel, array $sDeliveryInfo)
	        $bDecodeParamsAssoc = false,
	        $bReQueue = false
        ) {

            $this->setOnExitHandler();

            $this->setOnPhpErrorHandler();

            $endPoint= new QueueHandler($oConfig);
            $endPoint->makeOneQueuePerMethod(true);
            $endPoint->handleQueue(
	            $this->getType(),
	            $sClassName,
	            $onSuccess,
	            $onError,
	            $sQueueMethod, // Job name
	            $onJobAck,
	            $onJobReject,
                $bDecodeParamsAssoc,
	            $bReQueue
            );

        }

		/**
		 * @param $sMessage
		 * @throws \Workshop\Application\Event\Exception
		 */
		public function raiseTicker($sMessage) {
			$this->oEventHandler->raiseEvent(self::TICKER_EVENT, $sMessage, $this);
		}

		/**
		 * @param \Closure $pClosure
		 * @throws \Workshop\Application\Event\Exception
		 */
		public function listenToTicker(\Closure $pClosure) {
			$this->oEventHandler->addEventListener(self::TICKER_EVENT, $pClosure);
		}

        /**
         * Default onPhpError handler
         *
         * @return \Closure
         */
        protected function onPhpError() {
            return function ($sErrorSeverity, $sErrorMessage, $sErrorFile, $sErrorLine) {
                // error was suppressed with the @-operator
                if (0 === error_reporting()) {
                    return false;
                }
                throw new \Exception($sErrorMessage, 0);
            };
        }

		/**
		 * Consumes configured queue.
		 *
		 * @param QueueConfig $oConfig Queue configuration
		 * @param null $sClassName
		 * @param \Closure $onSuccess Closure in form of function(bool result, array methodParams)
		 * @param \Closure $onError Runs on error
		 * @param string | null $sJobName Override queue name . By default queue name equals class name.
		 * @param \Closure $onJobAck Replaces standard basic_ack
		 * @param \Closure $onJobReject Replaces standard basic_reject
		 * @param bool $bDecodeParamsAssoc
		 * @param bool $bReQueue
		 */
        public function consumeQueue(
	        QueueConfig $oConfig,
	        $sClassName = null,
	        \Closure $onSuccess = null,
	        \Closure $onError = null,
	        $sJobName = null,
	        \Closure $onJobAck= null,  // function($mJobResult, \PhpAmqpLib\Channel\AMQPChannel $oRabbitMqChannel, array $sDeliveryInfo)
	        \Closure $onJobReject = null, // function(\Exception $oException, \PhpAmqpLib\Channel\AMQPChannel $oRabbitMqChannel, array $sDeliveryInfo)
	        $bDecodeParamsAssoc = false,
	        $bReQueue = false
        ) {

            $this->setOnExitHandler();

            $this->setOnPhpErrorHandler();

            $endPoint= new QueueHandler($oConfig);
            $endPoint->handleQueue(
	            get_class($this),
	            $sClassName,
	            $onSuccess,
	            $onError,
	            $sJobName,
	            $onJobAck,
	            $onJobReject,
		        $bDecodeParamsAssoc,
	            $bReQueue
            );

        }

        private function setOnExitHandler() {
            $onExitClosure = $this->onExit();
            if ($onExitClosure !== null) {
                register_shutdown_function($onExitClosure);
            }
        }

        private function setOnPhpErrorHandler() {
            $onPhpExitClosure = $this->onPhpError();
            if ($onPhpExitClosure !== null) {
                set_error_handler($onPhpExitClosure);
            }
        }

    }

}
